<?php

namespace App\Http\Controllers;

use App\Good;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Exception;

class GoodController extends Controller
{

       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the goods
     *
     * @param  \App\Good  $model
     * @return \Illuminate\View\View
     */
    public function index(Good $model)
    {
        return view('goods.index', ['goods' => $model->paginate(15)]);
    }

   
    public function create()
    {
        return view('goods.create');
    }

   
    public function store(Request $request)
    {
        try {
            $good = new Good();
            DB::transaction(function () use ($request, &$good) {
                $good->create($request->all());
            });
        } catch (Exception $e) {
            return redirect()->route('good.index')->withStatus(__($e->getMessage()));
        }
        return redirect()->route('good.index')->withStatus(__('Bien creado con éxito.'));    
    }
   
    public function edit(Good $good)
    {
        return view('goods.edit', compact('good'));
    }

   
    public function update(Request $request, Good  $good)
    {
        try {
            DB::transaction(function () use ($request, &$good) {
                $good->update($request->all());
            });
        } catch (Exception $e) {
            return redirect()->route('good.index')->withStatus(__($e->getMessage()));
        }
        return redirect()->route('good.index')->withStatus(__('Bien Actualizado con éxito.'));     
    }

    public function destroy(Good  $good)
    {
        $good->delete();
        return redirect()->route('good.index')->withStatus(__('Bien eliminado con éxito.'));
    }
}
